# Sample Makefile

CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra

# Links together files needed to create executable
main: main.o readLine.o
	$(CC) -o main main.o readLine.o

# Compiles main.c to create main.o
main.o: main.c readLine.h
	$(CC) $(CFLAGS) -c main.c

# Compiles readLine.c to create readLine.o
readLine.o: readLine.c readLine.h
	$(CC) $(CFLAGS) -c readLine.c

# Removes all object files and the executable named main,
# so we can start fresh
clean:
	rm -f *.o main
