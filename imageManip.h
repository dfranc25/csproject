#define IMAGEMANIP_H
#include <stdio.h>
#include <stdlib.h>
#include "ppmIO.h"

/* swap function */
Image* swap(Image* im);

/* blackout function */
Image* blackout(Image* im);

/* crop function */
Image* crop(Image* im);

/* grayscale function */
Image* grayscale(Image* im);

/* contrast function */ 
Image* contrast(Image* im);

