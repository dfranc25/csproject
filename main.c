#include "ppmIO.h"

int main( int argc , char* argv[] )
{
  Image* im;
  FILE* fp = NULL;
  FILE* out = NULL;// The file pointer from which we read
  // If no command line arguments are specified, fail.
  if( argc==1 )
    {
      fprintf( stderr , "[ERROR] Expected file name\n" );
      return 1;
    }
  // Otherwise, the first argument is the name of the file from which to read.
  else
    {
      fp = fopen( argv[1] , "rb" );
      if (!fp) {
	fprintf(stderr, "Unable to open file \n");
	return 1;
      }
      im = readPPM(fp);
    }
  fclose(fp);
  //save file name
  //then do their manips
  //to the image should be able to directly change the pointer
  //ask about that
  
  if(argv[2] != NULL){
    out = fopen(argv[2], "wb");
    if(!out){
      fprintf(stderr, "error");
      return 1;
    }
  }
  else{
    fprintf(stderr, "not able to output to a file");
  }
   //open file for output
  
  int gotten = writePPM(out, im);
  int isGot = im->cols * im->rows;
  if(isGot == gotten){
    fprintf(stdout, "Sucessful manipulation");
  }
  else{
    fprintf(stderr, "There was an issue");
  }
  fclose(out);

  
  free(im->data);
  free(im);
  return 0;
}
