//ppmIO.c
//601.220, Spring 2018
//Starter code for midterm project - feel free to edit/add to this file

#include <stdlib.h>
#include <stdio.h>
#include "ppmIO.h"

/* read PPM formatted image from a file (assumes fp != NULL) */
Image* readPPM( FILE *fp)
{
  char buff[16];
  Image *im;
  int c; 
  int rgb;
  
  //read image format
  if (!fgets(buff, sizeof(buff), fp)) {
    fprintf(stderr, "there was an issue with loading the first lines\n");
    exit(3);
  }

  //check the image format
  if (buff[0] != 'P' || buff[1] != '6') {
    fprintf(stderr, "Invalid image format\n");
    exit(3);
  }

  //alloc memory form image
  im = (Image*)malloc(sizeof(Image));
  if (!im) {
    fprintf(stderr, "memory does not exist\n");
    exit(8);
  }

  //check for comments
  c = fgetc(fp);
  //this checks doe the hash tag
  while (c == '#') {
    //this jsut ignores all the stfuff after it
    while (fgetc(fp) != '\n');
    c = fgetc(fp);
  }

  //read image size information
  if(fscanf(fp, " %d %d", &im->cols, &im->rows) != 2) {
    fprintf(stderr, "Invalid image size\n");
    exit(3);
  }
  
  im->cols += 10 * 10 * (c - '0');
  //read rgb component
  if (fscanf(fp, " %d", &rgb) != 1) {
    fprintf(stderr, "Invalid rgb\n");
    exit(3);
  }

  fgetc(fp);//this is to get that extra char in order to not have color off
  
  //check rgb component depth
  if (rgb!= 255) {
    fprintf(stderr, "does not have 8-bits components\n");
    exit(8);
  }

  im->data = (Pixel*)malloc(im->cols * im->rows * sizeof(Pixel));

  if (!im) {
    fprintf(stderr, "no memory\n");
    exit(8);
  }
  int count = im->rows * im->cols;
  fread(im->data, sizeof(Pixel), count, fp);
  
  return im;
}


/* write PPM formatted image to a file (assumes fp != NULL and img != NULL) */
int writePPM(FILE* fp, const Image* im) {
  if(fp == NULL){
    fprintf(stderr, "This file is NULL\n");
  }
  fprintf(fp, "P6\n"); // special header string
// write dimensions: note that cols come before rows in PPM format
  fprintf(fp, "%d %d\n%d\n", im->cols, im->rows, 255);
// write pixel data
  int nelts = im->rows * im->cols;
  int nwritten = 0;
       
  nwritten += (int)fwrite(im->data, sizeof(Pixel), nelts, fp);
    
  
  if(!fp || !im){
    return -1;
  }
  if(nwritten != nelts) {
    return -1;
  }
<<<<<<< HEAD
  
=======
  //got rid of the free(im) here to fix compiling error ~Savanna
>>>>>>> 85fff35b130b2ddc6de5f9cd7eae9fae39f85b56
  return nwritten;
 
}



